module gasprop
    export MWH2,MWN2,MWNH3,P,R,ρgas 
    MWH2 = 2.016
    MWN2 = 28
    MWNH3 = 17.031 
    P = 20      # pressure bar
    R = 8.314   # gas constant J/molK
    ρgas = 0.05 # gas density ton/m3 
end

# Reactor 
module Reactor 
    export Cpc,ΔH,L,D,V,nq,ρcat 
    Cpc = 1100    # catalyst heat capacity J/kgK
    ΔH = 2.7e6    # heat of reaction J/kgNH3
    L = [2 3 4]   # length of each bed, m 
    D = [0.7 0.7 0.7]  # diameter of each bed  m 
    V = L*π.*(D/2).^2 # bed volumes m^3
    nq = [0.057 0.2 0.2] #[0.230 0.139 0.127]   split fractions for quenching  (nominal)
    ρcat = 2200 # catalyst bulk density kg/m3    
end

# Heat exchangers 
module HEX
    export Cpw,Ah,Uh,Ac,Tw_out,Tw_in,Tabs
    Cpw = 4180  # water heat capacity @ 25 C J/kg K
    Ah = 300    #  heat exchanger area (HEX1 and HEX2) m2 
    Uh = 500    # heat transfer coefficient (HEX1, HEX2 and HEX3) W/m2 K  
    Ac = 300     # cooler heat transfer area (HEX3) m2 
    # - temperature of cooling water
    Tw_out = 130 + 273  # outlet 
    Tw_in  = 25 + 273   # inlet
    Tabs = 150 + 273  # temperature of absorbent inlet
end

# - values for gas heat capacity calculation
module Cpcons
    export C1,C2,C3
    C1 = [33.066178, -11.363417, 11.432816, -2.772874, -0.158558, -9.980797, 172.707974] # H2
    C2 = [19.50583, 19.88705, -8.598535, 1.369784, .527601, -4.935202, 212.39] # N2 
    C3 = [19.99563, 49.77119, -15.37599, 1.921168, .189174, -53.30667, -45.89806] # NH3
end

# Absorber 
module Absorber
    export MW,ϵb,Da,Aa,La,ρabs,dp,Va,qmax,τf,B,μg,ϵp,Dkn,Dbk,rp,peq
    using ..gasprop
    using ..HEX
    MW = [MWN2 MWH2 MWNH3]
    # Tabs = 150 + 273  # temperature of absorbent inlet
    ϵb = 0.32   # bed void fraction 
    Da = 3.2  # cross-sectional diameter  m
    Aa = π*(Da/2)^2  # cross-sectional area of the absorber m
    La = 6    # absorber length m 
    ρabs = 3.72 # absorbent density ton/m3 
    dp = 4e-4   # particle diameter m 
    Va = La*Aa  # volume of the column m3
    qmax = 25.2
    # - parameters for Peq 
    Pref = 1    # bar 
    ΔHabs = 87000  # heat of absorption J/mol
    Tref  = 648.05  # K 
    peq = Pref*exp(-(ΔHabs/R)*(1/Tabs - 1/Tref))

    # - parameters for absoortpion kinetics 
    rp = dp/2
    rpore = 2e-10         # pore radius, m 
    ϵp = 0.117            # intraparticle voidage 
    τp = 1.2              # tortuosity of the absorbent particle 
    B  = 1                # Langmuir constant
    τf  = τp + 1.5*(1-ϵp)  # tortuosity factor 
    μg = 0.0764           # gas viscosity, kg/m h
    Dkn = 97*rpore*(Tabs/MWNH3)^(0.5) # Knudsen diffusion coefficient, m²/s
    # Axial effective dispersion coefficients calculations will be calculated at the inlet conditions 
    #   see books: "Gas separation by adsoprtion processes" p 107 and 
    #              "Principles of adsorption and adsorption processes" p 213


    # - molecular diffusivities, mole fractions at the input are considered
    #   - atomic and structural diffusion-volume increments
    #     see Welty's book
    #     1: N2, 2: H2, 3: NH3
    ni = Array{Float64}(undef,3)
    ni[1] = 17.9    # N2 
    ni[2] = 7.07    # H2 
    ni[3] = 11.63   # NH3 (5.69 + 3*1.98)

    Pa = P/1.013    # pressure in atm (1atm = bar/1.013)

    #   - binary diffusivities cm2/s
    Dbk = zeros(3,3)
    for j in 1:3,k in 1:3
        if k == j
            continue
        else
            Dbk[j,k] = (sqrt(1/MW[j] + 1/MW[k])*0.001*Tabs^1.75)/(Pa*(ni[j]^(1/3) + ni[k]^(1/3))^2)
        end
    end
end;


# =====================================================================================================

# ====================================== Simulation parameters ========================================
module simu
    export nb, nr, nt, M, tf, Δzr, h, σ1, σ2, na, Δza
    using ..Reactor
    using ..Absorber 
    # Reactor 
    nb = 3   # Number of beds
    nr = 11  # Number of discrete spatial points (for each bed) plus the beginning 
    nt = 30  # Number of discrete time points
    M = 3    # Number of collocation points plus the beginning 
    t0 = 0.0 # initial time
    tf = 4 # final time (hours)
    Δzr = Array{Float64}(undef,nb)
    Δzr .= 1/(nr-1) # spatial discrete steps for the reactor (each bed) 
    h = Array{Float64}(undef,nt)
    h .= tf/nt # size of finite elements (for both units)
    σ1 = 1e-4   # control weight u1
    σ2 = 1e-4   # control weight u2
    # Absorber 
    na = 10  # Number of discrete spatial points (internal points)
    Δza = La/(na+1) # spatial discrete steps for the absorber 
end;

# Sets
module sets 
    export bb, ni, nn_r, nn_a, mj, mj2, mk
    using ..simu
    bb = 1:nb       # beds
    ni = 1:nt       # Finite elements
    nn_r  = 1:nr   # Number of ode's for the reactor 
    nn_a  = 1:na   # Number of ode's for the absorber 
    mj = 1:M    # Collocation points plus the beginning point
    mj2 = 2:M   # Collocation points only
    mk = copy(mj)
end;

# Collocation matrix and points and quadrature weights
module colloc
    export Ω,Ψ,τ,w,r
    using ..simu
    # (Radau points)
    include("matrix.jl")
    Ω,Ψ,τ = colmatrix(M,"Radau")

    include("Rtableau.jl")      # It provides the weights for approximating the integral
    A,w,r = butcher(M)
end;