# DynOptRxAbsNH3

This repository contains the Julia files that accompany the paper:
"Optimal operation of a reaction-absorption process for ammonia production at low pressure"

The dynamic optimization can be run by executing the file main_program.jl. 
