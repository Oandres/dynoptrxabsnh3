# - distance (including both boundaries z=0 and z=L)
z1r = range(start=0,step=Δzr[1],length=nr)
z2r = range(start=0,step=Δzr[2],length=nr)
z3r = range(start=0,step=Δzr[3],length=nr)

za = range(start=0,step=Δza,length=na+2)

# - time
t = Array{Float64}(undef,nt*M)
for i in ni
	for j in mj
      if i == 1
         tpre = 0
      else
         tpre = t[M*(i-1)]
      end
      t[j+M*(i-1)] = τ[j]*h[i] + tpre
	end
end

t = t*60


plt1 = subplots()
subplot(221)
plot_surface(t,z1r*L[1],Tr[1,:,:],cmap="coolwarm",linewidth=0,antialiased=false)
xlabel("t [min]",fontsize=12)
ylabel("z (bed length)",fontsize=12)
zlabel("T ᵒC (bed 1)",fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
subplot(222)
plot_surface(t,z2r*L[2],Tr[2,:,:],cmap="coolwarm",linewidth=0,antialiased=false)
xlabel("t [min]",fontsize=12)
ylabel("z (bed length)",fontsize=12)
zlabel("T ᵒC (bed 2)",fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
subplot(223)
plot_surface(t,z3r*L[3],Tr[3,:,:],cmap="coolwarm",linewidth=0,antialiased=false)
xlabel("t [min]",fontsize=12)
ylabel("z (bed length)",fontsize=12)
zlabel("T ᵒC (bed 3)",fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
tight_layout()

plt2 = subplots()
subplot(221)
plot_surface(t,z1r*L[1],CNH3r[1,:,:],cmap="coolwarm",linewidth=0,antialiased=false)
xlabel("t [min]",fontsize=12)
ylabel("z (bed length)",fontsize=12)
zlabel("NH3 (bed 1)",fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
subplot(222)
plot_surface(t,z2r*L[2],CNH3r[2,:,:],cmap="coolwarm",linewidth=0,antialiased=false)
xlabel("t [min]",fontsize=12)
ylabel("z (bed length)",fontsize=12)
zlabel("NH3 (bed 2)",fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
subplot(223)
plot_surface(t,z3r*L[3],CNH3r[3,:,:],cmap="coolwarm",linewidth=0,antialiased=false)
xlabel("t [min]",fontsize=12)
ylabel("z (bed length)",fontsize=12)
zlabel("NH3 (bed 3)",fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
tight_layout()



plt2 = subplots()
subplot(221)
plot_surface(t,za,CN2a,cmap="coolwarm",linewidth=0,antialiased=false)
xlabel("t [min]",fontsize=12)
ylabel("z (bed length)",fontsize=12)
zlabel("N2",fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
subplot(222)
plot_surface(t,za,CH2a,cmap="coolwarm",linewidth=0,antialiased=false)
xlabel("t [min]",fontsize=12)
ylabel("z (bed length)",fontsize=12)
zlabel("H2",fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
subplot(223)
plot_surface(t,za,CNH3a,cmap="coolwarm",linewidth=0,antialiased=false)
xlabel("t [min]",fontsize=12)
ylabel("z (bed length)",fontsize=12)
zlabel("NH3",fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
tight_layout()


figure()
plot(t,[Qus[1,:] Qus[2,:] Qus[3,:]],linewidth=2)
xlabel("t, min",fontsize=12)
legend([L"q_1",L"q_2",L"q_3"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
tight_layout()

dummy = Array{Float64}(undef,nt*M)
dummy .= NaN

dummy2 = Array{Float64}(undef,nr)
dummy2 .= NaN

figure()
plot(t,rf,linewidth=2,color="royalblue")
plot(t,m1s,linewidth=2,color="darkorange")
plot(t,dummy,linewidth=2,linestyle="--",color="darkred")
axis([-7.5,252,0.84,36.62])
xlabel("t, min",fontsize=12)
ylabel("ton/h",fontsize=12)
legend(["recycle flow rate","feed flow rate","feed temperature"],fontsize=12)
ax = gca()
ax2 = ax[:twinx]()
plot(t,T1s,linewidth=2,linestyle="--",color="darkred")
ylabel("°C",fontsize=12)
axis([-7.5,252,9.5,394.5])
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
setp(ax2[:get_yticklabels](),fontsize=12)
tight_layout()


figure()
plot(t,ObjF,linewidth=2,color="darkgreen")
plot(t,dummy,linewidth=2,color="darkorange")
xlabel("t, min",fontsize=12)
ylabel("ton/h",fontsize=12)
legend(["absorbed NH₃","NH₃ mass fraction"],fontsize=12,loc=7)
ax = gca()
ax2 = ax[:twinx]()
plot(t,CNH3r[nb,nr,:],linewidth=2,color="darkorange")
ylabel("mass fraction",fontsize=12)
axis([-7.5,252,0.0,0.6])
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
setp(ax2[:get_yticklabels](),fontsize=12)
tight_layout()


T12s = Tr[nb,nr,:]

figure()
plot(t,[T12s T7s T8s T13s],linewidth=2)
xlabel("t, min",fontsize=12)
ylabel("°C")
legend(["T₁₂","T₇","T₈","T₁₃"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
tight_layout()

figure()
plot(t,[T2s T3s T13s T14s],linewidth=2)
xlabel("t, min",fontsize=12)
ylabel("°C")
legend(["T₂","T₃","T₁₃","T₁₄"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)
tight_layout()


