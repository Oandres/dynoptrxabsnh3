# This Julia file contains the model of the reactor-absorber for 
# optimization 


De = Array{Any}(undef,3)
Dm = Array{Any}(undef,3)
Pe = Array{Any}(undef,3)
αlo = Array{Any}(undef,3)
αmd = Array{Any}(undef,3)
αhi = Array{Any}(undef,3)
yp = Array{Any}(undef,3,3)
y_ai = Array{Any}(undef,3)
mi0 =Array{Any}(undef,nb)

yN2i = Yi[1]
yH2i = Yi[2]
yNH3i = Yi[3]

cNH3_i = MWNH3*yNH3i/(MWH2*yH2i+MWN2*yN2i+MWNH3*yNH3i) # mass fraction of NH3 in the inlet 
cH2_i = MWH2*yH2i/(MWH2*yH2i+MWN2*yN2i+MWNH3*yNH3i) # mass fraction of H2 in the inlet 
cN2_i = MWN2*yN2i/(MWH2*yH2i+MWN2*yN2i+MWNH3*yNH3i) # mass fraction of N2 in the inlet 

ra = Model(Ipopt.Optimizer)

# Variables
# - reactor 
@variable(ra,0.5<=cN2r[b in bb,n in nn_r,i in ni,j in mj]<=0.9,start = C_rx[1][b,n,i,j])    # N2 mass concentration
@variable(ra,0.1<=cH2r[b in bb,n in nn_r,i in ni,j in mj]<=0.2,start = C_rx[2][b,n,i,j])   # H2 mass concentration
@variable(ra,550<=T[b in bb,n in nn_r,i in ni,j in mj]<=800,start = T_rx[1][b,n,i,j])       # reactor temperature 
@variable(ra,500<=T3[i in ni,j in mj]<=700,start = T_rx[2][i,j])
@variable(ra,500<=T13[i in ni,j in mj]<=700,start = T_rx[3][i,j])
# - absorber 
@variable(ra,0.001<=cN2a[n in nn_a,i in ni,j in mj]<=1,start = C_abs[1][n,i,j])    # N2 mass fraction
@variable(ra,0.001<=cH2a[n in nn_a,i in ni,j in mj]<=0.3,start = C_abs[2][n,i,j])  # H2 mass fraction
@variable(ra,0<=cNH3a[n in nn_a,i in ni,j in mj]<=0.1,start = C_abs[3][n,i,j]) # NH3 mass fraction
@variable(ra,0<=q[n in nn_a,i in ni,j in mj]<=15,start=q_abs[1][n,i,j])           # absorbed/desorbed NH3 


# - manipulated
@variable(ra,1<=m17[i in ni,j in mj]<=35,start = M17g[1][i,j])                          # recycle (control var)
@variable(ra,300<=T1[i in ni,j in mj]<=653,start = T1g[1][i,j])                         # feed temperature
@variable(ra,1<=m1[i in ni,j in mj]<=35,start = M1g[1][i,j])                            # feed mass flow rate m1
@variable(ra,0<= qu[b in bb,i in ni,j in mj] <= maximum(Qu_g[b]),start = Qu_g[b][i,j])  # split fraction to each bed 
# ==================================  Auxiliary expressions ====================================

# - this function calculates the heat capacity given the temperature and mass fractions of each component 
function Cp(Temp,N2,H2,NH3)
    CP = (H2*(C1[1] + C1[2]*(Temp/1000) + C1[3]*(Temp/1000)^2 
        + C1[4]*(Temp/1000)^3 + C1[5]/(Temp/1000)^2)*1000/MWH2 
        + N2*(C2[1] + C2[2]*(Temp/1000) + C2[3]*(Temp/1000)^2 
        + C2[4]*(Temp/1000)^3 + C2[5]/(Temp/1000)^2)*1000/MWN2 
        + NH3*(4.184*(6.5846 - 0.61251e-2*Temp + 0.23663e-5*Temp^2 
        - 1.5981e-9*Temp^3 + (96.1678-0.067571*P*0.98692) + (-0.2225 + 1.6847e-4*P*0.98692)*Temp 
                                                + (1.289e-4 - 1.0095e-7*P*.98692)*Temp^2))*1000/MWNH3)
    return CP                                                
end                                               
register(ra,:Cp,4,Cp,autodiff=true)

# - switching function for heat exchanger efficiency 
function ntu(cr)
    if cr >= 1-1e-3
        return 0.0
    else
      return 1.0
    end
end
register(ra,:ntu,1,ntu,autodiff=true)

@NLexpression(ra,m1tot,sum(h[i]*w[j-1]*m1[i,j] for i in ni,j in mj2))


# - mass flow rate of stream 2
@NLexpression(ra,m2[i in ni,j in mj],m1[i,j] + m17[i,j])

# - temperature of mix (stream 2)
@NLexpression(ra,Cpg_17[i in ni,j in mj],Cp(Tabs,cN2a[na,i,j],cH2a[na,i,j],cNH3a[na,i,j])) 
@NLexpression(ra,Cpg_1[i in ni,j in mj],Cp(T1[i,j],cN2_i,cH2_i,cNH3_i)) 
@NLexpression(ra,T2[i in ni,j in mj],
    (Cpg_17[i,j]*m17[i,j]*Tabs + Cpg_1[i,j]*m1[i,j]*T1[i,j])/(Cpg_17[i,j]*m17[i,j]+Cpg_1[i,j]*m1[i,j]))


# - mas fractions of mixture (stream 2)
@NLexpression(ra,cN2i[i in ni,j in mj],cN2_i)
@NLexpression(ra,cH2i[i in ni,j in mj],cH2_i)
@NLexpression(ra,cNH3i[i in ni,j in mj],cNH3_i)


# - quench mass flows
@NLexpression(ra,mq[b in bb,i in ni,j in mj],qu[b,i,j]*m2[i,j])

# - flow through exchanger (mf = m7, m3 = m2)
@NLexpression(ra,mf[i in ni,j in mj],(1 - sum(qu[b,i,j] for b in bb))*m2[i,j])

# - mass flow rate through each bed
mi0[1] = @NLexpression(ra,[i in ni,j in mj],mf[i,j] + mq[1,i,j])
mi0[2] = @NLexpression(ra,[i in ni,j in mj],mf[i,j] + mq[1,i,j] + mq[2,i,j])
mi0[3] = @NLexpression(ra,[i in ni,j in mj],mf[i,j] + mq[1,i,j] + mq[2,i,j] + mq[3,i,j])

# - NH3 mass fraction
@NLexpression(ra,cNH3r[b in bb,n in nn_r,i in ni,j in mj],1-(cH2r[b,n,i,j]+cN2r[b,n,i,j]))

# - mas fractions at the reactor outlet (streams 12, 13, 14, 15)
@NLexpression(ra,N2r[i in ni,j in mj],cN2r[3,nr,i,j])
@NLexpression(ra,H2r[i in ni,j in mj],cH2r[3,nr,i,j])
@NLexpression(ra,NH3r[i in ni,j in mj],cNH3r[3,nr,i,j])

# - gas heat capacity (this expression simplifies the notation for Cpg in the reactor)   
@NLexpression(ra,Cpg[b in bb,n in nn_r,i in ni,j in mj],Cp(T[b,n,i,j],cN2r[b,n,i,j],cH2r[b,n,i,j],cNH3r[b,n,i,j])) 
    
    
# - heat exchanger HEX 2 (m13 = m14 = m15 = mi0[3][i,j]) 
#   - heat capacity of inlet cold stream (stream 2)
@NLexpression(ra,Cpg_2[i in ni,j in mj],Cp(T2[i,j],cN2i[i,j],cH2i[i,j],cNH3i[i,j])) 

#   - heat capacity of inlet hot stream (stream 13)
@NLexpression(ra,Cpg_13[i in ni,j in mj],Cp(T13[i,j],N2r[i,j],H2r[i,j],NH3r[i,j])) 

#   - heat capacity of oulet cold stream (stream 3)
@NLexpression(ra,Cpg_3[i in ni,j in mj],Cp(T3[i,j],cN2i[i,j],cH2i[i,j],cNH3i[i,j])) 

#   - Cmin J/h K
@NLexpression(ra,Cmin_2[i in ni,j in mj],1000*min(mi0[3][i,j]*Cpg_13[i,j],m2[i,j]*Cpg_2[i,j])) 

#   - Cmax J/h K
@NLexpression(ra,Cmax_2[i in ni,j in mj],1000*max(mi0[3][i,j]*Cpg_13[i,j],m2[i,j]*Cpg_2[i,j])) 

#   - NTU 
@NLexpression(ra,NTU_2[ i in ni,j in mj],3600*Uh*Ah/Cmin_2[i,j]) 

#   - heat capacity ratio 
@NLexpression(ra,Cr_2[i in ni,j in mj],Cmin_2[i,j]/Cmax_2[i,j])

#   - heat exchanger efficiency
# @NLexpression(ra,ϵ_2[i in ni,j in mj],((1-exp(-NTU_2[i,j]*(1-Cr_2[i,j])))/(1-Cr_2[i,j]*exp(-NTU_2[i,j]*(1-Cr_2[i,j]))))*ntu(Cr_2[i,j])
#                                          + (NTU_2[i,j]/(1+NTU_2[i,j]))*(1-ntu(Cr_2[i,j])))
#   - if the switching function is not used 
@NLexpression(ra,ϵ_2[i in ni,j in mj],NTU_2[i,j]/(1+NTU_2[i,j]))                                         

#   - heat being transferred 
@NLexpression(ra,q_2[i in ni,j in mj],ϵ_2[i,j]*Cmin_2[i,j]*(T13[i,j]-T2[i,j]))

#   - temperature of outlet hot stream (stream 14)
@NLexpression(ra,T14[i in ni,j in mj],T13[i,j] 
                                    - q_2[i,j]/(1000*mi0[3][i,j]*Cpg_13[i,j]))


# - heat exchanger HEX 1 
#   - heat capacity of inlet cold stream (stream 7), same as Cp of stream 3
@NLexpression(ra,Cpg_7[i in ni,j in mj],Cpg_3[i,j]) 

#   - heat capacity of inlet hot stream (stream 12)
@NLexpression(ra,Cpg_12[i in ni,j in mj],Cp(T[3,nr,i,j],N2r[i,j],H2r[i,j],NH3r[i,j])) 

#   - Cmin J/h K
@NLexpression(ra,Cmin_1[i in ni,j in mj],1000*min(mi0[3][i,j]*Cpg_12[i,j],mf[i,j]*Cpg_7[i,j])) 

#   - Cmax J/h K
@NLexpression(ra,Cmax_1[i in ni,j in mj],1000*max(mi0[3][i,j]*Cpg_12[i,j],mf[i,j]*Cpg_7[i,j])) 

#   - NTU 
@NLexpression(ra,NTU_1[ i in ni,j in mj],3600*Uh*Ah/Cmin_1[i,j]) 

#   - heat capacity ratio 
@NLexpression(ra,Cr_1[i in ni,j in mj],Cmin_1[i,j]/Cmax_1[i,j])

#   - heat exchanger efficiency
# @NLexpression(ra,ϵ_1[i in ni,j in mj],((1-exp(-NTU_1[i,j]*(1-Cr_1[i,j])))/(1-Cr_1[i,j]*exp(-NTU_1[i,j]*(1-Cr_1[i,j]))))*ntu(Cr_1[i,j])
#                                          + (NTU_1[i,j]/(1+NTU_1[i,j]))*(1-ntu(Cr_1[i,j])))
#   - if the switching function is not used
@NLexpression(ra,ϵ_1[i in ni,j in mj],NTU_1[i,j]/(1+NTU_1[i,j]))        

#   - heat being transferred (T7 = T3)
@NLexpression(ra,q_1[i in ni,j in mj],ϵ_1[i,j]*Cmin_1[i,j]*(T[3,nr,i,j]-T3[i,j])) 


# - temperature of outlet cold stream (stream 8)
@NLexpression(ra,T8[i in ni,j in mj],T3[i,j] + q_1[i,j]/(1000*mf[i,j]*Cpg_7[i,j]))

# - cooler HEX3 
#   - heat capacity of inlet hot stream (stream 14)
@NLexpression(ra,Cpg_14[i in ni,j in mj],Cp(T14[i,j],N2r[i,j],H2r[i,j],NH3r[i,j])) 

#   - ΔT
@NLexpression(ra,ΔT1[i in ni,j in mj],T14[i,j] - Tw_out)
@NLexpression(ra,ΔT2,Tabs - Tw_in)

#   - LMTD
@NLexpression(ra,ΔTm[i in ni,j in mj],(ΔT1[i,j] - ΔT2)/log(ΔT1[i,j]/ΔT2))

#   - heat being transferred
@NLexpression(ra,q_3[i in ni,j in mj],Uh*Ac*ΔTm[i,j])

#   - water mass flow rate ton/h (13 <= mw <= 41 for T14 ~ 241 C)
@NLexpression(ra,mw[i in ni,j in mj],3.6*q_3[i,j]/(Cpw*(Tw_out - Tw_in)))

# - Absorber 
#   *axial dispersion coefficients are considered constant (calculated at inlet conditions)
#   - gas velocity at the inlet m/h
@NLexpression(ra,vg[i in ni,j in mj],mi0[3][i,j]/(Aa*ϵb*ρgas))   

# - absorber outlet mass flow rate ton/h 
@NLexpression(ra,m16[i in ni,j in mj],vg[i,j]*ϵb*Aa*ρgas)

#   - mole fractions at the inlet (y_ai = [yN2i yH2i yNH3i])

y_ai[1] = @NLexpression(ra,[i in ni,j in mj],
                       (N2r[i,j]/MWN2)*(1/(NH3r[i,j]/MWNH3+N2r[i,j]/MWN2+H2r[i,j]/MWH2))) 
y_ai[2] = @NLexpression(ra,[i in ni,j in mj], 
                        (H2r[i,j]/MWH2)*(1/(NH3r[i,j]/MWNH3+N2r[i,j]/MWN2+H2r[i,j]/MWH2)))
y_ai[3] = @NLexpression(ra,[i in ni,j in mj],1-(y_ai[1][i,j]+y_ai[2][i,j]))                                               

#   - diffusivities of each component into the mixture
for l in 1:3,k in 1:3
    if k == l
        yp[l,k] = @NLexpression(ra,[i in ni,j in mj],0.0)
    else
        yp[l,k] = @NLexpression(ra,[i in ni,j in mj],y_ai[k][i,j]/(1-y_ai[l][i,j]))
    end
end
for l in 1:3
    Dm[l] = @NLexpression(ra,[i in ni,j in mj],0.36/(sum(yp[l,k][i,j]/Dbk[l,k] for k in 1:3 if k!=l)))  # m/h
end
#   - effective dispersion coefficients (ϵDe/Dm = 20 + 0.5*Sc*Re m2/h)
for k in 1:3
    De[k] = @NLexpression(ra,[i in ni,j in mj],20*Dm[k][i,j]/ϵb + 0.5*vg[i,j]*dp)
end

#   - Peclet (local) numbers (1: N2, 2: H2, 3: NH3)
for k in 1:3
    Pe[k] = @NLexpression(ra,[i in ni,j in mj],vg[i,j]*Δza/De[k][i,j])
end
#   - additional parameters (α's can be calculated using vg for simplicity)
for k in 1:3
    αlo[k] = @NLexpression(ra,[i in ni,j in mj],vg[i,j]/Δza + De[k][i,j]/Δza^2)
    αmd[k] = @NLexpression(ra,[i in ni,j in mj],-vg[i,j]/Δza - 2De[k][i,j]/Δza^2)
    αhi[k] = @NLexpression(ra,[i in ni,j in mj],De[k][i,j]/Δza^2)
end

# - parameters for the kinetic model (for ammonia only)
@NLexpression(ra,Re[i in ni,j in mj],vg[i,j]*ϵb*dp*1000*ρgas/μg)
@NLexpression(ra,Sc[i in ni,j in mj],μg/(1000*Dm[3][i,j]*ρgas))
@NLexpression(ra,Sh[i in ni,j in mj],2 + 1.1*(Sc[i,j]^(1/3))*(Re[i,j]^(0.6)))
@NLexpression(ra,kf[i in ni,j in mj],Sh[i,j]*Dm[3][i,j]/(3600*dp))
@NLexpression(ra,Kp[i in ni,j in mj],1/(τf*(1/Dkn + 3600/Dm[3][i,j])))
@NLexpression(ra,MTC[i in ni,j in mj],1/(rp/(2*kf[i,j]) + (rp^2)/(15*ϵp*Kp[i,j])))

# - mix temperatures 
    #   - heat capacity of stream 8
@NLexpression(ra,Cpg_8[i in ni,j in mj],Cp(T8[i,j],cN2i[i,j],cH2i[i,j],cNH3i[i,j])) 
    #   - heat capacity of bed 1 outlet stream
@NLexpression(ra,Cpg_b1[i in ni,j in mj],Cp(T[1,nr,i,j],cN2r[1,nr,i,j],cH2r[1,nr,i,j],cNH3r[1,nr,i,j])) 
    #   - heat capacity of bed 2 outlet stream
@NLexpression(ra,Cpg_b2[i in ni,j in mj],Cp(T[2,nr,i,j],cN2r[2,nr,i,j],cH2r[2,nr,i,j],cNH3r[2,nr,i,j])) 

@NLexpression(ra,T10[i in ni,j in mj],
    (Cpg_8[i,j]*mf[i,j]*T8[i,j] + Cpg_3[i,j]*mq[1,i,j]*T3[i,j])/(Cpg_8[i,j]*mf[i,j]+Cpg_3[i,j]*mq[1,i,j]))

@NLexpression(ra,T20[i in ni,j in mj],
    (Cpg_b1[i,j]*mi0[1][i,j]*T[1,nr,i,j] + Cpg_3[i,j]*mq[2,i,j]*T3[i,j])/(Cpg_b1[i,j]*mi0[1][i,j]+Cpg_3[i,j]*mq[2,i,j]))

@NLexpression(ra,T30[i in ni,j in mj],
    (Cpg_b2[i,j]*mi0[2][i,j]*T[2,nr,i,j] + Cpg_3[i,j]*mq[3,i,j]*T3[i,j])/(Cpg_b2[i,j]*mi0[2][i,j]+Cpg_3[i,j]*mq[3,i,j])) 

# - mass concentrations after quenching
@NLexpression(ra,cN210[i in ni,j in mj],cN2i[i,j]*mf[i,j]/(mf[i,j]+mq[1,i,j]) + cN2i[i,j]*mq[1,i,j]/(mf[i,j]+mq[1,i,j]))
@NLexpression(ra,cN220[i in ni,j in mj],cN2r[1,nr,i,j]*mi0[1][i,j]/(mi0[1][i,j]+mq[2,i,j]) 
                                                                        + cN2i[i,j]*mq[2,i,j]/(mi0[1][i,j]+mq[2,i,j]))
@NLexpression(ra,cN230[i in ni,j in mj],cN2r[2,nr,i,j]*mi0[2][i,j]/(mi0[2][i,j]+mq[3,i,j]) 
                                                                        + cN2i[i,j]*mq[3,i,j]/(mi0[2][i,j]+mq[3,i,j]))

@NLexpression(ra,cH210[i in ni,j in mj],cH2i[i,j]*mf[i,j]/(mf[i,j]+mq[1,i,j]) + cH2i[i,j]*mq[1,i,j]/(mf[i,j]+mq[1,i,j]))
@NLexpression(ra,cH220[i in ni,j in mj],cH2r[1,nr,i,j]*mi0[1][i,j]/(mi0[1][i,j]+mq[2,i,j]) 
                                                                        + cH2i[i,j]*mq[2,i,j]/(mi0[1][i,j]+mq[2,i,j]))
@NLexpression(ra,cH230[i in ni,j in mj],cH2r[2,nr,i,j]*mi0[2][i,j]/(mi0[2][i,j]+mq[3,i,j]) 
                                                                        + cH2i[i,j]*mq[3,i,j]/(mi0[2][i,j]+mq[3,i,j]))

# - molar fractions (reactor)
@NLexpression(ra,yN2r[b in bb,n in nn_r,i in ni,j in mj],
                                (cN2r[b,n,i,j]/MWN2)*(1/(cNH3r[b,n,i,j]/MWNH3+cN2r[b,n,i,j]/MWN2+cH2r[b,n,i,j]/MWH2)))     

@NLexpression(ra,yH2r[b in bb,n in nn_r,i in ni,j in mj],
                                (cH2r[b,n,i,j]/MWH2)*(1/(cNH3r[b,n,i,j]/MWNH3+cN2r[b,n,i,j]/MWN2+cH2r[b,n,i,j]/MWH2)))   
                                
@NLexpression(ra,yNH3r[b in bb,n in nn_r,i in ni,j in mj],1-(yH2r[b,n,i,j]+yN2r[b,n,i,j]))

# - velocities 
@NLexpression(ra,uw[b in bb,n in nn_r,i in ni,j in mj],
            mi0[b][i,j]*1000*Cpg[b,n,i,j]*3600/(Cpc*V[b]*ρcat*3600))

# - partial pressures (reactor)
@NLexpression(ra,pN2r[b in bb,n in nn_r,i in ni,j in mj],yN2r[b,n,i,j]*P)
@NLexpression(ra,pH2r[b in bb,n in nn_r,i in ni,j in mj],yH2r[b,n,i,j]*P)
@NLexpression(ra,pNH3r[b in bb,n in nn_r,i in ni,j in mj],yNH3r[b,n,i,j]*P)

# - k constants
@NLexpression(ra,k1[b in bb,n in nn_r,i in ni,j in mj],1.79e4*exp(-87090/(R*T[b,n,i,j])))
@NLexpression(ra,k2[b in bb,n in nn_r,i in ni,j in mj],2.57e16*exp(-198464/(R*T[b,n,i,j])))

# - reaction rate
@NLexpression(ra,r[b in bb,n in nn_r,i in ni,j in mj],4.75*(k1[b,n,i,j]*pN2r[b,n,i,j]*(pH2r[b,n,i,j]^(1.5))/pNH3r[b,n,i,j]
                                                    - k2[b,n,i,j]*pNH3r[b,n,i,j]/(pH2r[b,n,i,j]^(1.5)))/ρcat)

# - function f(T,c)
@NLexpression(ra,Fun[b in bb,n in nn_r,i in ni,j in mj2],ΔH*2*MWNH3*r[b,n,i,j]/Cpc)                                                    

# - RHS of the differential equations
@NLexpression(ra,Tdot[b in bb,n in 2:nr,i in ni,j in mj2],Fun[b,n,i,j] - uw[b,n,i,j]*(T[b,n,i,j]-T[b,n-1,i,j])/Δzr[b])

# - molar fractions (absorber)
@NLexpression(ra,yN2a[n in nn_a,i in ni,j in mj],
                                (cN2a[n,i,j]/MWN2)*(1/(cNH3a[n,i,j]/MWNH3+cN2a[n,i,j]/MWN2+cH2a[n,i,j]/MWH2)))     

@NLexpression(ra,yH2a[n in nn_a,i in ni,j in mj],
                                (cH2a[n,i,j]/MWH2)*(1/(cNH3a[n,i,j]/MWNH3+cN2a[n,i,j]/MWN2+cH2a[n,i,j]/MWH2)))   
                                
@NLexpression(ra,yNH3a[n in nn_a,i in ni,j in mj],
                                (cNH3a[n,i,j]/MWNH3)*(1/(cNH3a[n,i,j]/MWNH3+cN2a[n,i,j]/MWN2+cH2a[n,i,j]/MWH2)))

# - partial pressures (absorber)
@NLexpression(ra,pN2a[n in nn_a,i in ni,j in mj],yN2a[n,i,j]*P)
@NLexpression(ra,pH2a[n in nn_a,i in ni,j in mj],yH2a[n,i,j]*P)
@NLexpression(ra,pNH3a[n in nn_a,i in ni,j in mj],yNH3a[n,i,j]*P)   

@NLexpression(ra,q_eq[n in nn_a,i in ni,j in mj],qmax*B*pNH3a[n,i,j]/(1+B*pNH3a[n,i,j])) 

@NLexpression(ra,dqdt[n in nn_a,i in ni,j in mj],3600*MTC[i,j]*(q_eq[n,i,j]-q[n,i,j]))

@NLexpression(ra,Ra[n in nn_a,i in ni,j in mj],dqdt[n,i,j]*MWNH3*0.001*(ρabs/ρgas)*(1-ϵb)/ϵb)

# - RHS of the differential equations (absorber) (1: N2, 2: H2, 3: NH3)
#   - n = 1
@NLexpression(ra,cN2dot1[i in ni,j in mj2],(αmd[1][i,j]+αlo[1][i,j]/(1+Pe[1][i,j]))*cN2a[1,i,j] 
            + αlo[1][i,j]*Pe[1][i,j]*N2r[i,j]/(1+Pe[1][i,j]) + αhi[1][i,j]*cN2a[2,i,j])

@NLexpression(ra,cH2dot1[i in ni,j in mj2],(αmd[2][i,j]+αlo[2][i,j]/(1+Pe[2][i,j]))*cH2a[1,i,j] 
            + αlo[2][i,j]*Pe[2][i,j]*H2r[i,j]/(1+Pe[2][i,j]) + αhi[2][i,j]*cH2a[2,i,j])   
                    
@NLexpression(ra,cNH3dot1[i in ni,j in mj2],(αmd[3][i,j]+αlo[3][i,j]/(1+Pe[3][i,j]))*cNH3a[1,i,j] 
    + αlo[3][i,j]*Pe[3][i,j]*NH3r[i,j]/(1+Pe[3][i,j]) + αhi[3][i,j]*cNH3a[2,i,j] - Ra[1,i,j])

#   - n = 2,...,na-1
@NLexpression(ra,cN2dotn[n in 2:na-1,i in ni,j in mj2],αlo[1][i,j]*cN2a[n-1,i,j] + αmd[1][i,j]*cN2a[n,i,j] 
                                        + αhi[1][i,j]*cN2a[n+1,i,j])

@NLexpression(ra,cH2dotn[n in 2:na-1,i in ni,j in mj2],αlo[2][i,j]*cH2a[n-1,i,j] + αmd[2][i,j]*cH2a[n,i,j] 
                                        + αhi[2][i,j]*cH2a[n+1,i,j])

@NLexpression(ra,cNH3dotn[n in 2:na-1,i in ni,j in mj2],αlo[3][i,j]*cNH3a[n-1,i,j] + αmd[3][i,j]*cNH3a[n,i,j] 
                                        + αhi[3][i,j]*cNH3a[n+1,i,j] - Ra[n,i,j])

#   - n = na
@NLexpression(ra,cN2dotns[i in ni,j in mj2],αlo[1][i,j]*cN2a[na-1,i,j] + (αmd[1][i,j]+αhi[1][i,j])*cN2a[na,i,j])

@NLexpression(ra,cH2dotns[i in ni,j in mj2],αlo[2][i,j]*cH2a[na-1,i,j] + (αmd[2][i,j]+αhi[2][i,j])*cH2a[na,i,j])

@NLexpression(ra,cNH3dotns[i in ni,j in mj2],αlo[3][i,j]*cNH3a[na-1,i,j] + (αmd[3][i,j]+αhi[3][i,j])*cNH3a[na,i,j] 
                                                                                            - Ra[na,i,j])  
# - integrand
@NLexpression(ra,fk2[i in ni,j in mj],(mi0[3][i,j]*NH3r[i,j] - m16[i,j]*cNH3a[na,i,j]))
# ============================================================================================

# Objective function
@NLobjective(ra,Max,sum(h[i]*w[j-1]*(fk2[i,j] - (1e-4/2)*m1[i,j]^2 - (1e-5/2)*T1[i,j]^2 - (1e-6/2)*m17[i,j]^2 
                                                - (1e-5/2)*sum(qu[b,i,j]^2 for b in bb)) for i in ni,j in mj2))

# Constraints
# - orthgonal collocation 
#   - reactor
@NLconstraint(ra,hn[b in bb,n in 2:nr,i in ni,j in mj2],(1/h[i])*sum(Ω[j,k]*T[b,n,i,k] for k in mk) == Tdot[b,n,i,j])

#   - absorber
#       - n = 1
@NLconstraint(ra,cN2_1[i in ni,j in mj2],(1/h[i])*sum(Ω[j,k]*cN2a[1,i,k] for k in mk) == cN2dot1[i,j])
@NLconstraint(ra,cH2_1[i in ni,j in mj2],(1/h[i])*sum(Ω[j,k]*cH2a[1,i,k] for k in mk) == cH2dot1[i,j])
@NLconstraint(ra,cNH3_1[i in ni,j in mj2],(1/h[i])*sum(Ω[j,k]*cNH3a[1,i,k] for k in mk) == cNH3dot1[i,j])
#       - n 
@NLconstraint(ra,cN2_n[n in 2:na-1,i in ni,j in mj2],(1/h[i])*sum(Ω[j,k]*cN2a[n,i,k] for k in mk) == cN2dotn[n,i,j])
@NLconstraint(ra,cH2_n[n in 2:na-1,i in ni,j in mj2],(1/h[i])*sum(Ω[j,k]*cH2a[n,i,k] for k in mk) == cH2dotn[n,i,j])
@NLconstraint(ra,cNH3_n[n in 2:na-1,i in ni,j in mj2],(1/h[i])*sum(Ω[j,k]*cNH3a[n,i,k] for k in mk) == cNH3dotn[n,i,j])
#        - n = na
@NLconstraint(ra,cN2_ns[i in ni,j in mj2],(1/h[i])*sum(Ω[j,k]*cN2a[na,i,k] for k in mk) == cN2dotns[i,j])
@NLconstraint(ra,cH2_ns[i in ni,j in mj2],(1/h[i])*sum(Ω[j,k]*cH2a[na,i,k] for k in mk) == cH2dotns[i,j])
@NLconstraint(ra,cNH3_ns[i in ni,j in mj2],(1/h[i])*sum(Ω[j,k]*cNH3a[na,i,k] for k in mk) == cNH3dotns[i,j])

@NLconstraint(ra,qNH3[n in nn_a,i in ni,j in mj2],(1/h[i])*sum(Ω[j,k]*q[n,i,k] for k in mk) == dqdt[n,i,j])


# - Continuity
@NLconstraint(ra,contT[b in bb,n in 2:nr,i in 2:nt],T[b,n,i,1] == T[b,n,i-1,M])
@NLconstraint(ra,contN2[n in nn_a,i in 2:nt],cN2a[n,i,1] == cN2a[n,i-1,M])
@NLconstraint(ra,contH2[n in nn_a,i in 2:nt],cH2a[n,i,1] == cH2a[n,i-1,M])
@NLconstraint(ra,contNH3[n in nn_a,i in 2:nt],cNH3a[n,i,1] == cNH3a[n,i-1,M])
@NLconstraint(ra,contq[n in nn_a,i in 2:nt],q[n,i,1] == q[n,i-1,M])


# - algebraic equations
#   - concentrations in the reactor 
@NLconstraint(ra,N2[b in bb,n in 2:nr,i in ni,j in mj],-uw[b,n,i,j]*(cN2r[b,n,i,j]-cN2r[b,n-1,i,j])/Δzr[b]
                                        -Cpg[b,n,i,j]*MWN2*r[b,n,i,j]/Cpc == 0)
@NLconstraint(ra,H2[b in bb,n in 2:nr,i in ni,j in mj],-uw[b,n,i,j]*(cH2r[b,n,i,j]-cH2r[b,n-1,i,j])/Δzr[b]
                                        -Cpg[b,n,i,j]*3*MWH2*r[b,n,i,j]/Cpc == 0)

# - temperature of streams 3 and 13                                        
@NLconstraint(ra,T_3[i in ni,j in mj],T3[i,j] == T2[i,j] + q_2[i,j]/(1000*m2[i,j]*Cpg_2[i,j]))   

@NLconstraint(ra,T_13[i in ni,j in mj],T13[i,j] == T[3,nr,i,j] - q_1[i,j]/(1000*mi0[3][i,j]*Cpg_12[i,j]))

# - Boundary values 
#   - initial values for T
@NLconstraint(ra,Tiv[b in bb,n in 2:nr],T[b,n,1,1] == T0[b,n])
#   - values of T at the inlet of each bed
@NLconstraint(ra,Tb1[i in ni,j in mj],T[1,1,i,j] == T10[i,j])
@NLconstraint(ra,Tb2[i in ni,j in mj],T[2,1,i,j] == T20[i,j])
@NLconstraint(ra,Tb3[i in ni,j in mj],T[3,1,i,j] == T30[i,j])
#   - values of c at the inlet of each bed
@NLconstraint(ra,cN21[i in ni,j in mj],cN2r[1,1,i,j] == cN210[i,j])
@NLconstraint(ra,cN22[i in ni,j in mj],cN2r[2,1,i,j] == cN220[i,j])
@NLconstraint(ra,cN23[i in ni,j in mj],cN2r[3,1,i,j] == cN230[i,j])

@NLconstraint(ra,cH21[i in ni,j in mj],cH2r[1,1,i,j] == cH210[i,j])
@NLconstraint(ra,cH22[i in ni,j in mj],cH2r[2,1,i,j] == cH220[i,j])
@NLconstraint(ra,cH23[i in ni,j in mj],cH2r[3,1,i,j] == cH230[i,j])             

#   - initial values for c in the absorber 
@NLconstraint(ra,cN2iv[n in nn_a],cN2a[n,1,1] == C0[1][n])
@NLconstraint(ra,cH2iv[n in nn_a],cH2a[n,1,1] == C0[2][n])
@NLconstraint(ra,cNH3iv[n in nn_a],cNH3a[n,1,1] == C0[3][n])
@NLconstraint(ra,qiv[n in nn_a],q[n,1,1] == 0)


# - Control representation
# - constant function 
@NLconstraint(ra,uc1[i in ni,j in mj;j!=2],m1[i,j] == m1[i,2])
@NLconstraint(ra,uc2[i in ni,j in mj;j!=2],T1[i,j] == T1[i,2])
@NLconstraint(ra,uc3[i in ni,j in mj;j!=2],m17[i,j] == m17[i,2])

#   - additional constraints 
@NLconstraint(ra,posr[b in bb,n in nn_r,i in ni,j in mj],cNH3r[b,n,i,j] >= 0)
@NLconstraint(ra,m17U[i in ni,j in mj],m17[i,j] <= m16[i,j])
@NLconstraint(ra,mfL[i in ni,j in mj],mf[i,j]>=0.1)
@NLconstraint(ra,m2U[i in ni,j in mj],m2[i,j] <= 36)
@NLconstraint(ra,uc4[b in bb,i in ni,j in mj;j!=2],qu[b,i,j] == qu[b,i,2])

# - constraints on the split
@NLconstraint(ra,sq[i in ni,j in mj;j!=2],sum(qu[b,i,j] for b in bb) + mf[i,j]/m2[i,j] == 1)

