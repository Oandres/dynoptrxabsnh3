# - results 
cN2rs = value.(cN2r).data
CN2r = Array{Float64}(undef,nb,nr,nt*M)
for b in bb,n in nn_r
    CN2r[b,n,:] = transpose(cN2rs[b,n,:,:])[:]
end

cH2rs = value.(cH2r).data
CH2r = Array{Float64}(undef,nb,nr,nt*M)
for b in bb,n in nn_r
    CH2r[b,n,:] = transpose(cH2rs[b,n,:,:])[:]
end

cNH3rs = value.(cNH3r).data
CNH3r = Array{Float64}(undef,nb,nr,nt*M)
for b in bb,n in nn_r
    CNH3r[b,n,:] = transpose(cNH3rs[b,n,:,:])[:]
end

Ts = value.(T).data .- 273
Tr = Array{Float64}(undef,nb,nr,nt*M)
for b in bb,n in nn_r
    Tr[b,n,:] = transpose(Ts[b,n,:,:])[:]
end

qus = value.(qu).data 
Qus = Array{Float64}(undef,nb,nt*M)
for b in bb
    Qus[b,:] = transpose(qus[b,:,:])[:]
end

# - results 
cN2as = value.(cN2a).data
CN2a = Array{Float64}(undef,na+2,nt*M)
CN2a[1,:] .= transpose(value.(N2r).data)[:]
for n in 2:na+1
    CN2a[n,:] = transpose(cN2as[n-1,:,:])[:]
end
CN2a[na+2,:] .= CN2a[na+1,:]

cH2as = value.(cH2a).data
CH2a = Array{Float64}(undef,na+2,nt*M)
CH2a[1,:] .= transpose(value.(H2r).data)[:]
for n in 2:na+1
    CH2a[n,:] = transpose(cH2as[n-1,:,:])[:]
end
CH2a[na+2,:] .= CH2a[na+1,:]

cNH3as = value.(cNH3a).data
CNH3a = Array{Float64}(undef,na+2,nt*M)
CNH3a[1,:] .= transpose(value.(NH3r).data)[:]
for n in 2:na+1
    CNH3a[n,:] = transpose(cNH3as[n-1,:,:])[:]
end
CNH3a[na+2,:] .= CNH3a[na+1,:]


ObjF = transpose(value.(fk2).data)[:]


rf = transpose(value.(m17).data)[:]

T1s = transpose(value.(T1).data)[:] .- 273

m1s = transpose(value.(m1).data)[:]

mws = transpose(value.(mw).data)[:]

# HEX temperatures 
# - HEX 1 (T7 = T3)
T7s = transpose(value.(T3).data)[:] .- 273
T8s = transpose(value.(T8).data)[:] .- 273

# - HEX 2
T13s = transpose(value.(T13).data)[:] .- 273
T14s = transpose(value.(T14).data)[:] .- 273
T3s = transpose(value.(T3).data)[:] .- 273
T2s = transpose(value.(T2).data)[:] .- 273

M17g= Dict{Int64,Array{Float64}}()
T1g = Dict{Int64,Array{Float64}}() 
M1g = Dict{Int64,Array{Float64}}()
Qu_g = Dict{Int64,Array{Float64}}()


M17g[1] = value.(m17).data 
T1g[1]  = value.(T1).data 
M1g[1]  = value.(m1).data 

for b in bb
    Qu_g[b] = value.(qu[b,:,:]).data
end

