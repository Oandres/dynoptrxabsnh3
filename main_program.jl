# This Julia file contains the dynamic optimization of the reaction-absorption system
# that maximizes the NH3 production
#
# - A linear driving force kinetic model is used for the absorption/desorption rates
# - Constant gas velocity is assumed as the concentration of NH3 is very low
# - It is assumend that the recycle concentration is the same as that of the make-up gas
#
# Oswaldo Andres-Martinez
# andr1015@umn.edu 
# May 2023
    

using JuMP, Ipopt, PyPlot, DelimitedFiles, JLD

# This file contains all the necessary modules with 
#  model parameteres and operating conditions
include("Modules.jl")

# inlet conditions 
using ..simu
Yi = Dict{Int64,Float64}()
Yi[3] = 0.0      # inlet mole fraction NH₃ 
Yi[1] = 0.249899 # inlet mole fraction N₂
Yi[2] = 0.750101 # inlet mole fraction H₂

# initial conditions
# - temperature along the reactor 
T0 = (290 + 273)*ones(nb,nr)     
# - mass fractions along the absorber 
C0 = Dict{Int64,Array{Float64}}()
C0[1] = 0.001*ones(na)
C0[2] = 0.001*ones(na)
C0[3] = zeros(na)



using .gasprop
using .HEX 
using .Absorber
using .sets
using .colloc
using .Reactor 
using .Cpcons 

# - Initial guess values 
C_rx = load("C_rx.jld")["data"]
T_rx = load("T_rx.jld")["data"]
C_abs = load("C_abs.jld")["data"]
q_abs = load("q_abs.jld")["data"]
M17g = load("M17g.jld")["data"]
T1g = load("T1g.jld")["data"]
M1g = load("M1g.jld")["data"]
Qu_g = load("Qu_g.jld")["data"]


include("plant_model.jl")

# Solution
JuMP.optimize!(ra)

# Results
include("results.jl")

# Plotting
close("all")
include("plots.jl")
